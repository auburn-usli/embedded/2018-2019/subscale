
/*
 Name:		flightcode.ino
 Created:	7/3/2018 6:47:02 PM
 Authors:	
    Austen LeBeau
*/

// Add your name to authors if/when you add stuff.  

#include "vehicle.h"
#include "filter.h"
#include "datalog.h"


// I'd like to use a kalman filter for at least altFilter but I'll let you integrate it 
// into the code Carter
RollingAverage altFilter = RollingAverage();
RollingAverage accelFilter = RollingAverage();

Altimiter altimiter = Adafruit_MPL3115A2();
Accelerometer accelerometer = Adafruit_ADXL345_Unified(12345);

// We want to start off with idle on the launchpad.
Runmode MODE = idle;

// Go ahead and load these bad boys up.
State::alt = get_alt(altimiter);
altFilter.push(State::alt);
accelFilter.push(get_accel(accelerometer));


// the setup function runs once when you press reset or power the board
void setup() {

    Wire.begin();

    // Initialize the sensor 
    Serial.begin(9600);

    // Testing sensors
    if (!altimiter.begin()) {
        Serial.println("Altimiter failed to start. Check the wiring.");
        while (1); // Hang the program
    }

    if (!accelerometer.begin()) {
        Serial.println("Accelerometer failed to start. Check the wiring.");
        while (1);
    }
    // Set range to 4 g's, since when the rocket is coasting the total acceleration shouldn't
    // be more than 2.5 g's
    accelerometer.setRange(ADXL345_RANGE_4_G); 

}

// the loop function runs over and over again until power down or reset
void loop() {

    // Sitting on the launch pad.
    if (MODE == idle) {
        if (get_accel(accelerometer) > 0) {
            // Condition in this case is is the acceleration greater than 0
            auto condition = []() -> bool { return get_accel(accelerometer) > GRAV + 2.5; };
            // Passing it to verify_switch, which checks if the the condition has
            // been met for longer than 0.5 seconds
            if (verify_switch(condition)) MODE = launch;
        } 
        // This will hopefully switch the state to launch if the acceleration doesn't trigger it 
        // for some reason
        else if (abs(get_alt(altimiter) - State::alt) > 50) MODE = launch;
    }

    // Motor is burning, have to wait until burn out.
    else if (MODE == launch) {
        if (get_accel(accelerometer) < 0) {
            // Same thing as above
            auto condition = []() -> bool { return get_accel(accelerometer) < 0; };
            if (verify_switch(condition)) MODE = coast;
        }
    }

    else if (MODE == coast) {

        // Logs data after 50 milliseconds has passed
        double startTime = State::currentTime;
        if (fifty_millis(startTime, Millis())) {
            State::rawAccel = get_accel(accelerometer);
            State::rawAlt = get_alt(altimiter);
            State::accel = accelFilter.output();
            State::alt = altFilter.output();
            State::veloc = get_veloc();
            State::projAlt = projected_altitude(State::veloc, State::accel, State::alt);
            State::currentTime = Millis();
            
            // Load up all the data points
            data_array[0] = State::currentTime;
            data_array[1] = State::accel;
            data_array[2] = State::veloc;
            data_array[3] = State::projAlt;
            data_array[4] = State::alt;
            data_array[5] = State::rawAlt;
            data_array[6] = State::rawAccel;
            print_data(); // Write data to file
            pop_data();   // Clear data array 
            lastAlt = State::alt;
        }

        // Keep the averages rolling
        altFilter.push(State::rawAlt);
        accelFilter.push(State::rawAccel);

        // Reached apogee, stop doing stuff now
        if (get_veloc() < 0) {
            MODE = descent;
        }
        
    }

    else if (MODE == descent) {
        // Retract the fairings, close the data file, and power down after all that's done.
        if (dataFile) {
            close_file();
        }
    }
}
