#ifndef VEHICLE_H
#define VEHICLE_H
#include "math.h"
#include <Adafruit_MPL3115A2.h>
#include <Adafruit_ADXL345_U.h>
// #include <RTClib.h>

// Typedefing these so they're a bit clearer
typedef Adafruit_ADXL345_Unified Accelerometer;
typedef Adafruit_MPL3115A2 Altimiter;

/* Millis stuff that I found */
// System currentTime stuff
// volatile uint32_t counter = 0;
// SysTick_Config(SystemCoreClock / 1000);

// SysTickHandler(void) {
//     counter++;
// }

uint32_t Millis() {
    uint32_t counter = 0;
    return counter;
}

// State of the vehicle
// Made it a namespace since I was running into variable name issues
namespace State {
    double currentTime, accel, veloc, alt, rawAlt, rawAccel, projAlt;
}

/* Global vars */
double P;
double I;
double D;
double P_0;            // Proportional term.
double I_0;            // Integral term.
double timeDiff;       // Difference between loops, needed for proportional term.
double timerOne;
double timerTwo;
double lastAlt;

/* ALL THE CONSTANTS THE CONTROLLER NEEDS TO FUNCTION */
/* KP, KI and KD gathered from Simulink simulations */
/* Yes KP and KI are supposed to be negative */
const double KP = -0.5;          // Proportional gain.
const double KI = -0.25;         // Integral gain.
const double KD = 0.1;           // Derivative gain.
const int I_MAX = 100;           // Prevents integral windup.
const double SETPOINT = 5280;    // Target altitude in feet.

const short MAX = 175;          // Maximum input to motor.
const short MIN = -15;          // Minimum input to motor.

const double GRAV = 32.1737;    // Gravity constant in english units.
const double METERTOFEET = 3.280839895;


// Using an enum since it takes less space than strings and is clearer than just numbers
// I think it's clearer than what we did last year, where we had loops inside the main loop.
enum Runmode : unsigned char {
    idle,		// Rocket is sitting on launchpad
    launch,		// Motor is ignited and burning (have to wait for burnout)
    coast,		// Motor is burnt out, this is where the fairings will do their thing
    descent		// Rocket is now descending - retract fairings and power down
};


// Calculates the time between each loop, so we can use it to find velocity.
void setTimeDiff() {
    timerTwo = Millis();
    timeDiff = (timerTwo - timerOne) / 1000;
    timerOne = timerTwo;
}

// This is our max altitude, which is what the pid controller uses to calculate 
// the position of the plates. You can find the equations used here:
// https://www.grc.nasa.gov/www/k-12/airplane/flteqs.html. Actually simplified
// the equation so it's a bit shorter.
double projected_altitude(double veloc, double accel, double currentAlt) {
    double velocSqr = veloc * veloc;
    return (-velocSqr / (2 * (accel + GRAV))) * log(-accel / GRAV) + currentAlt;
}   


// The actual controller.
int pid_main(double position, double altitude, double velocity, double accel) {

    double projAlt = projected_altitude(velocity, accel, altitude);

    P = projAlt - SETPOINT;             // Proportional term.

    I = I_0 + (P * timeDiff);           // Integral term.

    if (I > I_MAX) I = I_MAX;           // These if statements are here to
    else if (I < -I_MAX) I = -I_MAX;    // prevent integral windup.

    D = (P - P_0) / 2;  // Derivative term.

    // diffOutput is the change in plate position, so we add it to the old position.
    double diffOutput = (P * KP) + (I * KI) + (D * KD);

    // This block saves the variables in this loop so it can be used in the next.
    P_0 = P;
    I_0 = I;
    lastAlt = State::alt;

    // updating the position of the plates.
    position += diffOutput;

    // These prevent the new position of the plates from going over their max and min values.
    if (position > MAX) { position = MAX; } else if (position < MIN) { position = MIN; }

    return (int) position;
}



// Has 50 milliseconds passed?
bool fifty_millis(double startTime, double currentTime) {
    return (currentTime - startTime) >= 50;
}

// Get the current altitude
double get_alt(Altimiter & sensor) {   
    return sensor.getAltitude() * METERTOFEET;
}

// Get the current acceleration (only in the up direction)
double get_accel(Accelerometer & sensor) {
    sensors_event_t event;
    sensor.getEvent(&event);
    return event.acceleration.z * METERTOFEET;
}

// Get the velocity.
double get_veloc() {
    setTimeDiff();
    return (State::alt - lastAlt) / timeDiff;
}

/* 
 * This is messy I know, basically passing in a function and acceleromter object.
 * The reason this function is here is to check if the rocket is actually launching, 
 * or the motor burned out, etc
 */
bool verify_switch(bool (*condition)()) {
    bool switchState;
    double endTime;
    double startTime = Millis() / 1000;
    while (1) {
        if (condition()) {
            endTime = Millis() / 1000;
            if (abs(endTime - startTime) >= 0.5) {
                switchState = true; // Actually doing the thing
                break;
            }
        } else if (!condition(Accel)) {
            switchState = false; // Actually did not do the thing
            break;
        }
    }
    return switchState;
}

#endif // VEHICLE_H
